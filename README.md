# users-cli

## Descripción

Comando para realizar operaciones sobre usuarios (Java 1.8)

create -> crea un nuevo usuario

list -> lista los usuarios existentes

edit -> edita un usuario existente por login

delete -> elimina un usuario existente por login


## Configuración

La aplicación se conecta a un base de datos Mongodb en localhost en el puerto 27017.

Esto se puede cambiar modificando el fichero de configuración src/main/resources/config.properties antes de generar el jar:

````
db.mongo.host=localhost
db.mongo.port=27017
````

## Generación del jar ejecutable

```bash
$ mvn package
```

El fichero se genera en la ruta target/users-cli-1.0-shaded.jar

## Ejecución

```bash
$ cd target

# Registrar un nuevo usuario
$ java -jar users-cli-1.0-shaded.jar create <login> <password> <nombre>

# Listar los usuarios existentes
$ java -jar users-cli-1.0-shaded.jar list

# Editar un usuario existente (por login)
$ java -jar users-cli-1.0-shaded.jar edit <login> <password> <nombre>

# Borrar un usuario (por login)
$ java -jar users-cli-1.0-shaded.jar delete <login>
```

## Test

```bash
# Test unitarios
$ mvn test

```

