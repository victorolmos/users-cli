package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.WrongParamNumberException;
import com.victorolmos.users_cli.repository.UsersRepository;

public class DeleteUsersCommand implements UsersCommand {

    private UsersRepository repository;

    public DeleteUsersCommand(final UsersRepository repository) {
        this.repository = repository;
    }

    public void execute(String[] args) throws Exception {
        if (args.length != 1) {
            throw new WrongParamNumberException(1, args.length);
        }
        final User user = this.repository.findByLogin(args[0]);
        this.repository.deleteById(user.getId());
        System.out.println("Se ha eliminado correctamente el siguiente usuario:\n" + user.toString());
    }

}
