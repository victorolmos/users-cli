package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.WrongParamNumberException;
import com.victorolmos.users_cli.repository.UsersRepository;

import java.util.Collection;

public class ListUsersCommand implements UsersCommand {

    private UsersRepository repository;

    public ListUsersCommand(final UsersRepository repository) {
        this.repository = repository;
    }

    public void execute(String[] args) throws Exception {
        if (args.length != 0) {
            throw new WrongParamNumberException(0, args.length);
        }
        final Collection<User> users = this.repository.findAll();
        for (User user : users) {
            System.out.println(user.toString());
        }
    }
}
