package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.exceptions.CommandNotExistsException;
import com.victorolmos.users_cli.repository.UsersRepository;

/**
 * Factoría de commands, añadir en el método "create" los nuevos comandos.
 * El constructor recibe el repositorio a usar por los comandos.
 */
public class UsersCommandFactory {

    private UsersRepository repository;

    public UsersCommandFactory(final UsersRepository repository) {
        this.repository = repository;
    }

    public UsersCommand create(final String command) throws CommandNotExistsException {
        if ("create".equals(command)) {
            return new CreateUsersCommand(this.repository);
        }
        if ("list".equals(command)) {
            return new ListUsersCommand(this.repository);
        }
        if ("edit".equals(command)) {
            return new EditUsersCommand(this.repository);
        }
        if ("delete".equals(command)) {
            return new DeleteUsersCommand(this.repository);
        }
        throw new CommandNotExistsException(command);
    }

}
