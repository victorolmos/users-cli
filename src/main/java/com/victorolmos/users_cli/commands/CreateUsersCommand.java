package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.WrongParamNumberException;
import com.victorolmos.users_cli.repository.UsersRepository;
import com.victorolmos.users_cli.services.MD5CryptoService;

public class CreateUsersCommand implements UsersCommand {

    private UsersRepository repository;

    public CreateUsersCommand(final UsersRepository repository) {
        this.repository = repository;
    }

    public void execute(String[] args) throws Exception {
        if (args.length != 3) {
            throw new WrongParamNumberException(3, args.length);
        }
        final User user = new User();
        user.setLogin(args[0]);
        user.setPassword(MD5CryptoService.crypt(args[1]));
        user.setName(args[2]);
        final String userId = this.repository.create(user);
        user.setId(userId);
        System.out.println("Se ha creado correctamente el siguiente usuario:\n" + user.toString());
    }
}
