package com.victorolmos.users_cli.commands;

public interface UsersCommand {

    void execute(String [] args) throws Exception;

}
