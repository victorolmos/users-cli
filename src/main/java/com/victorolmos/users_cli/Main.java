package com.victorolmos.users_cli;

import com.victorolmos.users_cli.commands.UsersCommand;
import com.victorolmos.users_cli.commands.UsersCommandFactory;
import com.victorolmos.users_cli.exceptions.BaseUsageException;
import com.victorolmos.users_cli.exceptions.DatabaseConnectionException;
import com.victorolmos.users_cli.repository.MongoUsersRepository;
import com.victorolmos.users_cli.repository.UsersRepository;
import com.victorolmos.users_cli.services.ConfigService;

public class Main {

    public static void main(final String[] inputArgs) {
        try {
            exec(inputArgs);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    private static void exec(final String[] inputArgs) throws Exception {
        final UsersRepository repository = createRepositoryByProperties();
        final UsersCommandFactory commandFactory = new UsersCommandFactory(repository);
        if (inputArgs.length == 0) {
            throw new BaseUsageException();
        }
        UsersCommand command = commandFactory.create(inputArgs[0]);
        command.execute(createCommandArgsByInputArgs(inputArgs));
    }

    /**
     * Obtine el repositorio a partir de la configuración del fichero "src/resources/config.properties".
     * De momento solo para Mongodb
     *
     * @return UsersRepository
     * @throws DatabaseConnectionException
     */
    private static UsersRepository createRepositoryByProperties() throws DatabaseConnectionException {
        final String mongoHost = ConfigService.getValueByKey("db.mongo.host");
        final int mongoPort = Integer.parseInt(ConfigService.getValueByKey("db.mongo.port"));
        return new MongoUsersRepository(mongoHost, mongoPort);
    }

    /**
     * Genera un array de argumentos sin la primera posición de la entrada para usarlo en los commands
     *
     * @param inputArgs
     * @return String[]
     */
    private static String[] createCommandArgsByInputArgs(final String[] inputArgs){
        String[] commandArgs = new String[inputArgs.length-1];
        System.arraycopy(inputArgs, 1, commandArgs, 0, commandArgs.length);
        return commandArgs;
    }
}
