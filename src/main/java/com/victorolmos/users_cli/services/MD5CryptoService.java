package com.victorolmos.users_cli.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5CryptoService {

    public static String crypt(final String input) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
           // No debería entrar nunca
        }
        return bytesToHexString(md.digest(input.getBytes()));
    }

    private static String bytesToHexString(final byte[] byteCrypted) {
        StringBuffer hexString = new StringBuffer();
        for (int i=0;i<byteCrypted.length;i++) {
            String hex=Integer.toHexString(0xff & byteCrypted[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
