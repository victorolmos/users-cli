package com.victorolmos.users_cli.services;

import java.util.ResourceBundle;

public class ConfigService {

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("config");

    public static String getValueByKey(final String key) {
        return RESOURCE_BUNDLE.getString(key);
    }

}
