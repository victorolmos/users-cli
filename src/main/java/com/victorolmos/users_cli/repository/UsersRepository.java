package com.victorolmos.users_cli.repository;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.UserAlreadyExistsException;
import com.victorolmos.users_cli.exceptions.UserNotExistsException;

import java.util.Collection;

public interface UsersRepository {

    String create(User user) throws UserAlreadyExistsException;

    User findByLogin(String userLogin) throws UserNotExistsException;

    Collection<User> findAll();

    void update(User user);

    void deleteById(String userId);

}
