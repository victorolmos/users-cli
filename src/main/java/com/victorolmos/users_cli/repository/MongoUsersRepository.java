package com.victorolmos.users_cli.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.DatabaseConnectionException;
import com.victorolmos.users_cli.exceptions.UserAlreadyExistsException;
import com.victorolmos.users_cli.exceptions.UserNotExistsException;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MongoUsersRepository implements UsersRepository {

    private static final String DATABASE_NAME = "users";
    private static final String COLLECTION_NAME = "users";
    private MongoDatabase db;

    public MongoUsersRepository(final String host, final int port) throws DatabaseConnectionException {
        Logger.getLogger( "org.mongodb.driver").setLevel(Level.SEVERE);
        MongoClient mongoClient = new MongoClient(new ServerAddress(host, port), this.getDefaultOptions());
        this.testConnection(mongoClient);
        this.db = mongoClient.getDatabase(DATABASE_NAME);
    }

    private MongoClientOptions getDefaultOptions() {
        return MongoClientOptions.builder().connectTimeout(5000).serverSelectionTimeout(5000).build();
    }

    private void testConnection(final MongoClient mongoClient) throws DatabaseConnectionException {
        try {
            mongoClient.getAddress();
        } catch (Exception e) {
            throw new DatabaseConnectionException();
        }
    }

    public String create(User user) throws UserAlreadyExistsException {
        try {
            this.findByLogin(user.getLogin());
            throw new UserAlreadyExistsException(user.getLogin());
        } catch (UserNotExistsException e) { /* Es el comportamiento esperado, el usuario no existe */ }
        user.setId(this.generateId());
        this.db.getCollection(COLLECTION_NAME).insertOne(this.userToDocument(user));
        return user.getId();
    }

    public User findByLogin(String userLogin) throws UserNotExistsException {
        FindIterable<Document> iterable = this.db.getCollection(COLLECTION_NAME).find(new BasicDBObject("login", userLogin));
        final Document document = iterable.first();
        if (document == null) {
            throw new UserNotExistsException(userLogin);
        }
        return this.documentToUser(document);
    }

    public Collection<User> findAll() {
        FindIterable<Document> iterable = this.db.getCollection(COLLECTION_NAME).find();
        final ArrayList<User> users = new ArrayList<User>();
        for (Document document : iterable) {
            users.add(this.documentToUser(document));
        }
        return users;
    }

    public void update(User user) {
        this.db.getCollection(COLLECTION_NAME).replaceOne(new BasicDBObject("_id", user.getId()),
                this.userToDocument(user));
    }

    public void deleteById(String userId) {
        this.db.getCollection(COLLECTION_NAME).deleteOne(new BasicDBObject("_id", userId));
    }

    private String generateId() {
        return new ObjectId().toHexString();
    }

    private Document userToDocument(final User user) {
        return new Document()
                .append("_id", user.getId())
                .append("login", user.getLogin())
                .append("password", user.getPassword())
                .append("name", user.getName());
    }

    private User documentToUser(final Document document) {
        final User user = new User();
        user.setId(document.getString("_id"));
        user.setLogin(document.getString("login"));
        user.setPassword(document.getString("password"));
        user.setName(document.getString("name"));
        return user;
    }
}
