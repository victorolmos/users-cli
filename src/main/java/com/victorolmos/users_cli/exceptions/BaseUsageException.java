package com.victorolmos.users_cli.exceptions;

public class BaseUsageException extends Exception {

    private static final String USAGE = "Uso del comando \"users\":" +
            "\nusers create <login> <password> <nombre>" +
            "\nusers list" +
            "\nusers edit <login> <password> <nombre>" +
            "\nusers delete <login>";

    public BaseUsageException() {
        super(USAGE);
    }
}
