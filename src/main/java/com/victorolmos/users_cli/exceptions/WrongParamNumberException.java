package com.victorolmos.users_cli.exceptions;

public class WrongParamNumberException extends Exception {

    public WrongParamNumberException(final int rightParamNumber, final int currentParamNumber) {
        super(String.format("Número de parámetros incorrecto\nRecibidos %d parámetros, se esperaban %d", currentParamNumber, rightParamNumber));
    }

}
