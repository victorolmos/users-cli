package com.victorolmos.users_cli.exceptions;

public class UserNotExistsException extends Exception {

    public UserNotExistsException(final String login) {
        super(String.format("El usuario con login \"%s\" no existe", login));
    }

}
