package com.victorolmos.users_cli.exceptions;

public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(final String login) {
        super(String.format("Ya existe un usuario con login \"%s\"", login));
    }

}
