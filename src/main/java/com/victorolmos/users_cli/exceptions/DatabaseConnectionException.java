package com.victorolmos.users_cli.exceptions;

public class DatabaseConnectionException extends Exception {

    public DatabaseConnectionException() {
        super("No se puede conectar a la base de datos, revise la configuración");
    }

}
