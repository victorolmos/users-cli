package com.victorolmos.users_cli.exceptions;

public class CommandNotExistsException extends Exception {

    public CommandNotExistsException(final String command) {
        super(String.format("El comando %s no existe", command));
    }

}
