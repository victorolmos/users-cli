package com.victorolmos.users_cli.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void equals() {
        final User user1 = new User();
        user1.setId("id");
        user1.setLogin("login");
        user1.setPassword("password");
        user1.setName("name");

        final User user2 = new User();
        user2.setId("id");
        user2.setLogin("login");
        user2.setPassword("password");
        user2.setName("name");

        assertEquals(user1, user2);
        user2.setName("name2");
        assertNotEquals(user1, user2);
    }

    @Test
    void toStringTest() {
        final User user = new User();
        user.setId("id");
        user.setLogin("login");
        user.setPassword("password");
        user.setName("name");

        assertEquals("{ login: 'login', password: '*****', name: 'name' }", user.toString());
    }
}