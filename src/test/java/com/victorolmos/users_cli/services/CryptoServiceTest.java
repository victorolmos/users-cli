package com.victorolmos.users_cli.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CryptoServiceTest {

    @Test
    void crypt() {
        assertEquals("4d186321c1a7f0f354b297e8914ab240", MD5CryptoService.crypt("hola"));
    }
}