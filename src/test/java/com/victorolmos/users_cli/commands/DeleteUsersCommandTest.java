package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.WrongParamNumberException;
import com.victorolmos.users_cli.repository.UsersRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeleteUsersCommandTest {

    private static final ByteArrayOutputStream OUT_CONTENT = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream ERROR_CONTENT = new ByteArrayOutputStream();

    @BeforeAll
    public static void setUpStreams() {
        System.setOut(new PrintStream(OUT_CONTENT));
        System.setErr(new PrintStream(ERROR_CONTENT));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    void executeWithLessArgs() {
        final UsersRepository repository = mock(UsersRepository.class);
        final DeleteUsersCommand command = new DeleteUsersCommand(repository);
        assertThrows(WrongParamNumberException.class, ()-> {
            command.execute(new String[]{});
        });
    }

    @Test
    void executeWithMoreArgs() {
        final UsersRepository repository = mock(UsersRepository.class);
        final DeleteUsersCommand command = new DeleteUsersCommand(repository);
        assertThrows(WrongParamNumberException.class, ()-> {
            command.execute(new String[]{"prueba", "prueba"});
        });
    }

    @Test
    void executeOk() throws Exception {
        final UsersRepository repository = mock(UsersRepository.class);
        final User user = new User();
        user.setId("id");
        user.setLogin("login");
        user.setPassword("password");
        user.setName("name");
        when(repository.findByLogin("login")).thenReturn(user);
        final DeleteUsersCommand command = new DeleteUsersCommand(repository);
        command.execute(new String[]{"login"});
        verify(repository).findByLogin(user.getLogin());
        verify(repository).deleteById(user.getId());
        assertEquals("Se ha eliminado correctamente el siguiente usuario:\n{ login: 'login', password: '*****', name: 'name' }\n",
                OUT_CONTENT.toString());
        assertTrue(ERROR_CONTENT.toString().isEmpty());
    }
}