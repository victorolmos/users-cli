package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.domain.User;
import com.victorolmos.users_cli.exceptions.WrongParamNumberException;
import com.victorolmos.users_cli.repository.UsersRepository;
import com.victorolmos.users_cli.services.MD5CryptoService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ListUsersCommandTest {

    private static final ByteArrayOutputStream OUT_CONTENT = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream ERROR_CONTENT = new ByteArrayOutputStream();

    @BeforeAll
    public static void setUpStreams() {
        System.setOut(new PrintStream(OUT_CONTENT));
        System.setErr(new PrintStream(ERROR_CONTENT));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    void executeWithMoreArgs() {
        final UsersRepository repository = mock(UsersRepository.class);
        final ListUsersCommand command = new ListUsersCommand(repository);
        assertThrows(WrongParamNumberException.class, ()-> {
            command.execute(new String[]{"prueba"});
        });
    }

    @Test
    void executeOk() throws Exception {
        final UsersRepository repository = mock(UsersRepository.class);
        final User user = new User();
        user.setId("id");
        user.setLogin("login");
        user.setPassword(MD5CryptoService.crypt("password"));
        user.setName("name");
        when(repository.findAll()).thenReturn(Arrays.asList(new User[]{user,user}));
        final ListUsersCommand command = new ListUsersCommand(repository);
        command.execute(new String[]{});
        verify(repository).findAll();
        assertEquals("{ login: 'login', password: '*****', name: 'name' }\n{ login: 'login', password: '*****', name: 'name' }\n",
                OUT_CONTENT.toString());
        assertTrue(ERROR_CONTENT.toString().isEmpty());
    }
}