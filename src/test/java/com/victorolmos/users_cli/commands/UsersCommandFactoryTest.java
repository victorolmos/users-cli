package com.victorolmos.users_cli.commands;

import com.victorolmos.users_cli.exceptions.CommandNotExistsException;
import com.victorolmos.users_cli.repository.UsersRepository;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class UsersCommandFactoryTest {

    @Test
    void createCommandCreate() throws CommandNotExistsException {
        final UsersCommand createCommand = createTestFactory().create("create");
        assertTrue(createCommand instanceof CreateUsersCommand);
    }

    @Test
    void createCommandDelete() throws CommandNotExistsException {
        final UsersCommand deleteCommand = createTestFactory().create("delete");
        assertTrue(deleteCommand instanceof DeleteUsersCommand);
    }

    @Test
    void createCommandEdit() throws CommandNotExistsException {
        final UsersCommand editCommand = createTestFactory().create("edit");
        assertTrue(editCommand instanceof EditUsersCommand);
    }

    @Test
    void createCommandList() throws CommandNotExistsException {
        final UsersCommand listCommand = createTestFactory().create("list");
        assertTrue(listCommand instanceof ListUsersCommand);
    }

    @Test
    void createUnknownCommand() throws CommandNotExistsException {
        assertThrows(CommandNotExistsException.class, ()-> {
            createTestFactory().create("otherCommand");
        });
    }

    private UsersCommandFactory createTestFactory() {
        final UsersRepository repository = mock(UsersRepository.class);
        return new UsersCommandFactory(repository);
    }

}